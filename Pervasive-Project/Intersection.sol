pragma solidity ^0.5.0;

contract Intersection {

	struct Booth {
		uint id;
		string name;
		uint VehicleCount;
	}
	
	

	uint public BoothCount;
	
	uint private Decision1;
	uint private Decision2;
	uint private Decision3;
	uint private Decision4;
	uint public Decision;
	uint public DecisionRd2;


	mapping(address => bool) public vehicles;

	mapping(uint => Booth) public booths;
	
	mapping(address => uint) public record;
	
	 

	constructor () public {
		addRegisterBooth("Booth 1");
		addRegisterBooth("Booth 2");
		addRegisterBooth("Booth 3");
		addRegisterBooth("Booth 4");
	}
	function addRegisterBooth (string memory _name) private {
		BoothCount++;
		booths[BoothCount] = Booth(BoothCount, _name,0);
	}

	function Register (uint _BoothId) public {

		require(!vehicles[msg.sender]);

		require(_BoothId > 0 && _BoothId <= BoothCount);

		 vehicles[msg.sender] = true;
		 
		 record[msg.sender]= _BoothId;

		 booths[_BoothId].VehicleCount ++;
		 
		 Decision1 = booths[1].VehicleCount;
		 Decision2 = booths[2].VehicleCount;
		 Decision3 = booths[3].VehicleCount;
		 Decision4 = booths[4].VehicleCount;
		 
		 if (Decision1 > Decision2  && Decision1 > Decision3 && Decision1 > Decision4  )
		    Decision= booths[1].id;
	    else if (Decision2 > Decision1  && Decision2 > Decision3 && Decision2 > Decision4  )
	        Decision= booths[2].id;
		 else if (Decision3 > Decision1  && Decision3 > Decision2 && Decision3 > Decision4  )
		    Decision= booths[3].id;
		 else if (Decision4 > Decision1  && Decision4 > Decision2 && Decision4 > Decision3  )
		    Decision= booths[4].id;
		 
		  if (Decision1 + Decision3  > Decision2 + Decision4  )
		     DecisionRd2 = 1;
		 else if (Decision1 + Decision3  < Decision2 + Decision4  )
		    DecisionRd2 = 2;
		 
	}
	
	function DeRegister (uint _BoothId) public {
	    
	    require(vehicles[msg.sender]);
	    
	    require(_BoothId > 0 && _BoothId <= BoothCount);
	    
	    require (record[msg.sender]==_BoothId);
	    
	    record[msg.sender]= 0;
	    
	    vehicles[msg.sender] = false;

		booths[_BoothId].VehicleCount --;
		
        Decision1 = booths[1].VehicleCount;
        Decision2 = booths[2].VehicleCount;
        Decision3 = booths[3].VehicleCount;
        Decision4 = booths[4].VehicleCount;
		 
		 if (Decision1 > Decision2  && Decision1 > Decision3 && Decision1 > Decision4  )
		    Decision= booths[1].id;
	    else if (Decision2 > Decision1  && Decision2 > Decision3 && Decision2 > Decision4  )
	        Decision= booths[2].id;
		 else if (Decision3 > Decision1  && Decision3 > Decision2 && Decision3 > Decision4  )
		    Decision= booths[3].id;
		 else if (Decision4 > Decision1  && Decision4 > Decision2 && Decision4 > Decision3  )
		    Decision= booths[4].id;
	    
	     if (Decision1 + Decision3  > Decision2 + Decision4  )
		     DecisionRd2 = 1;
		 else if (Decision1 + Decision3  < Decision2 + Decision4  )
		    DecisionRd2 = 2;
	}
	
	
	function getDecision() public view returns(uint)
	{
        return Decision;
    }
    
     function getDecisionRd2() public view returns(uint)
	{
        return DecisionRd2;
    }
    
    function getBoothName(uint _BoothId) public view returns(string memory)
    {
        return booths[_BoothId].name;
    }
    
    
    function getBoothVehicleCount(uint _BoothId) public view returns(uint)
    {
        return booths[_BoothId].VehicleCount;
    }
    
    function getRecord(address _vehicleAdd) public view returns(uint)
    {
        return record[_vehicleAdd];
    }
    
    function getRegisterStatus(address _vehicleAdd) public view returns(bool)
    {
        return vehicles[_vehicleAdd];
    }
}
